import React, { Component } from 'react';
import "./Lobby.css";
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import { List, ListItem } from 'material-ui/List';


class Lobby extends Component {
	constructor(props) {
		super(props);
		var self = this;
		this.state = {
			peopleList: []
		}
		var socket = this.props.socket;

		socket.emit('getUsersInLobby', function (data) {
			data = JSON.parse(data);
			var peopleList = [];
			for (var key in data) {
				peopleList.push({
					id: data[key],
					username: key
				});
			}
			self.setState({
				peopleList: peopleList
			});
		})

		socket.on('newPlayer', function (newPerson) {
			newPerson = JSON.parse(newPerson);
			self.setState({
				peopleList: self.state.peopleList.concat(newPerson)
			});
		});
		socket.on('gameStarted', function (person) {
			person = JSON.parse(person);
			console.log(person);
			socket.emit("gameStartAcknowledged", person.username)
			self.props.goToPage("BOARD", { type: "multiplayer", username: self.props.pageProps.username, opponent: person, playerType: "O" });
		})
		socket.on('playerLeft', function (person) {
			for (var i = 0; i < self.state.peopleList.length; ++i) {
				if (self.state.peopleList[i].id == person.id) {
					self.setState({
						peopleList: self.state.peopleList.concat([]).splice(i, 1)
					});
					break;
				}
			}
		});
	}
	personClicked(person) {
		var self = this;
		// this.props.socket.emit("leftlobby", this.props.pageProps.username);
		this.props.socket.emit("userSelected", person.username, function (data) {
			console.log("Selected");
			self.props.goToPage("BOARD", { type: "multiplayer", username: self.props.pageProps.username, opponent: person, playerType: "X" });
		})
	}
	render() {
		var peopleList = [];
		this.state.peopleList.forEach(person => {
			if (person.username !== this.props.pageProps.username)
				peopleList.push(<ListItem primaryText={person.username} onClick={this.personClicked.bind(this, person)} key={person.id} />);
		});
		return (
			<div className="lobby-container">
				<Card className="lobby-content">
					<div className="lobby-header">
						<h2>Find a person to play</h2>
					</div>
					<div>
						<List>
							{peopleList}
						</List>
					</div>
				</Card>
			</div>
		);
	}
}

export default Lobby;