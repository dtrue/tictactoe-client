import React, { Component } from 'react';
import "./Menu.css";
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';


class Menu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: "",
			showUsernameDialog: false
		}
	}
	goToMultiplayer() {
		this.setState({
			showUsernameDialog: true
		});
	}
	goToLobby() {
		this.props.goToPage("LOBBY", { username: this.state.username });
	}
	registerUsername() {
		var self = this;
		console.log("registreing", this.state.username);
		this.props.socket.emit("registerUsername", this.state.username, function (data) {
			if (data == "username-taken") {
				alert("Username already taken");
				return;
			}
			self.goToLobby();
		});
	}
	render() {
		var actions = [];
		if (this.state.username.length > 1) {
			actions = [
				<FlatButton
					label="OK"
					primary={true}
					onClick={this.registerUsername.bind(this)}
				/>
			]
		}
		return (
			<div className="menu-container">
				<Card className="menu-content">
					<div className="menu-header">
						<h2>Tic Tac Toe</h2>
					</div>
					<FlatButton label="Single Player" onClick={_ => { this.props.goToPage("BOARD", { type: "singleplayer" }) }} fullWidth={true} />
					<FlatButton label="Multiplayer" onClick={this.goToMultiplayer.bind(this)} fullWidth={true} />
				</Card>
				<Dialog
					title="Choose Username"
					modal={false}
					actions={actions}
					open={this.state.showUsernameDialog}
					onRequestClose={this.handleClose}
				>
					<TextField
						hintText="Username"
						onChange={(event) => this.setState({ username: event.target.value })}
					/>
				</Dialog>
			</div>
		);
	}
}

export default Menu;