import React, { Component } from 'react';
import "./Box.css";
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';


class Board extends Component {
	render() {
		return (
			<div className="box-container">
				<Card className="box-inner" onClick={this.props.onClick}>
					<span className="box-content">{this.props.content}</span>
				</Card>
			</div>
		);
	}
}

export default Board;