import React, { Component } from 'react';
import Box from "../Box/Box";
import "./Board.css";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

class Board extends Component {
	constructor(props) {
		super(props);
		var self = this;
		console.log(props);
		this.state = {
			boxValues: {},
			turn: "X",
			showGameOverDialog: false,
			gameOverDialogText: "",
			playerType: this.props.pageProps.type === "singleplayer" ? "X" : this.props.pageProps.playerType,
			type: this.props.pageProps.type
		}
		this.props.socket.on("opponentMoved", function (move) {
			self.boxClicked(move, "them");
		})
	}
	boxClicked(number, player) {
		if (player === "me" && this.state.playerType !== this.state.turn)
			return;
		var self = this;
		if (this.state.boxValues[number])
			return;
		var newState = {
			boxValues: Object.assign({}, this.state.boxValues, {
				[number]: this.state.turn
			}),
			turn: this.state.turn === "X" ? "O" : "X"
		}
		this.setState(newState);


		if (this.state.type === "multiplayer") {
			if (player === "me") {
				self.props.socket.emit("moved", number);
			}
		}
		//check if winner
		//only 8 possible combinations
		var winningCombinations = [
			[0, 1, 2], //horizontal
			[3, 4, 5],
			[6, 7, 8],

			[0, 3, 6], //vertical
			[1, 4, 7],
			[6, 7, 8],

			[0, 4, 8], //x
			[2, 4, 6]
		];

		var winner;
		var boxValues = newState.boxValues;
		winningCombinations.forEach(function (combination) {
			if (boxValues[combination[0]] === "X" && boxValues[combination[1]] === "X" && boxValues[combination[2]] === "X") {
				winner = "X";
			} else if (boxValues[combination[0]] === "O" && boxValues[combination[1]] === "O" && boxValues[combination[2]] === "O")
				winner = "O";
		});
		if (winner) {
			this.setState({
				showGameOverDialog: true,
				gameOverDialogText: this.state.playerType === winner ? "You win!" : "Sorry, but I'm afraid you lost"
			});
			return;
		}

		//check for tie
		var numTaken = Object.keys(boxValues).length;
		if (numTaken === 9) {
			this.setState({
				showGameOverDialog: true,
				gameOverDialogText: "Nobody won"
			});
			return;
		}

		//ai move
		if (this.state.type === "singleplayer" && newState.turn === "O") {
			console.log("is single player");
			var aiMove;
			var emptySpaces = [];
			for (var i = 0; i < 9; ++i) {
				if (!boxValues[i])
					emptySpaces.push(i);
			}
			var aiMove = emptySpaces[Math.floor(Math.random() * emptySpaces.length)];
			setTimeout(function () {
				console.log(aiMove);
				self.boxClicked(aiMove, newState.turn)
			}, Math.random() * 1000 * 0 + 1000) //make it look like it had to think a bit
		}
	}
	render() {
		const gameOverActions = [
			<FlatButton
				label="OK"
				primary={true}
				onClick={_ => this.props.goToPage("MENU")}
			/>,
		];
		return (
			<div className="board-component">
				<div className="board-container">
					<div className="board-row">
						<Box content={this.state.boxValues[0]} onClick={this.boxClicked.bind(this, 0, "me")} />
						<Box content={this.state.boxValues[1]} onClick={this.boxClicked.bind(this, 1, "me")} />
						<Box content={this.state.boxValues[2]} onClick={this.boxClicked.bind(this, 2, "me")} />
					</div>
					<div className="board-row">
						<Box content={this.state.boxValues[3]} onClick={this.boxClicked.bind(this, 3, "me")} />
						<Box content={this.state.boxValues[4]} onClick={this.boxClicked.bind(this, 4, "me")} />
						<Box content={this.state.boxValues[5]} onClick={this.boxClicked.bind(this, 5, "me")} />
					</div>
					<div className="board-row">
						<Box content={this.state.boxValues[6]} onClick={this.boxClicked.bind(this, 6, "me")} />
						<Box content={this.state.boxValues[7]} onClick={this.boxClicked.bind(this, 7, "me")} />
						<Box content={this.state.boxValues[8]} onClick={this.boxClicked.bind(this, 8, "me")} />
					</div>
				</div>
				<Dialog
					title="Game Over"
					modal={false}
					actions={gameOverActions}
					open={this.state.showGameOverDialog}
					onRequestClose={this.handleClose}
				>
					{this.state.gameOverDialogText}
				</Dialog>
			</div>
		);
	}
}


export default Board;
