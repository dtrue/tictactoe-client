import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import Board from "./Board/Board";
import Menu from "./Menu/Menu";
import Lobby from "./Lobby/Lobby";
import io from 'socket.io-client';

class App extends Component {

  constructor(props) {
    super(props);
    this.socket = io.connect('http://' + window.location.hostname + ':8000');
    this.state = {
      currentPage: "MENU"
    }
  }
  goToPage(page, pageProps) {
    this.setState({
      currentPage: page,
      pageProps
    })
  }
  render() {
    var self = this;
    function getPage() {
      switch (self.state.currentPage) {
        case "MENU":
          return <Menu goToPage={self.goToPage.bind(self)} socket={self.socket}/>
        case "BOARD":
          return <Board pageProps={self.state.pageProps} goToPage={self.goToPage.bind(self)} socket={self.socket}/>
        case "LOBBY":
          return <Lobby goToPage={self.goToPage.bind(self)} pageProps={self.state.pageProps} socket={self.socket}/>
      }
    }
    return (
      <MuiThemeProvider>
        <div className="main-content">
          {getPage()}
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
