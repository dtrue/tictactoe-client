# Welcome to the Tic Tac Toe application

This is a sample application that demonstrates a simple Tic Tac Toe game.  I (Dane) created it.

You can play single player or multi player!  In single player the AI plays you.

The multiplayer mode has a lobby where you can see anybody who is playing.  You can choose an opponent and play them from anywhere in the world!

There are two parts to this application.  Due to the fact that there is a multiplayer component and a library, there is also a small server application that handles the websockets and multiplayer game status.

https://bitbucket.org/dtrue/tictactoe-server/src/master/

Keep in mind that while reading the code that I built this with about a 4 hour time limit, so I had to make the right compromises in order to hit my target (knowing how to make the right compromise is an important skill)

As for application design, the thing is built with React and Node.js